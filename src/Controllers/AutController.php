<?php
namespace App\Controllers;
use \Firebase\JWT\JWT; 
use \App\Controllers\Miscelaneos as Misc;
//Controlador de Autorizacion
class AutController{
    static $key = 'prueba123';
    
    protected $container;
    public function __construct($container){
        $this->container = $container;
    }
    //Esta funcion recibe un Datos en forma de JSON o ARRAY Object y devuelve un Token JWT en forma de String
    //No es necesario especificar los valores "iat" y "exp" ya que la funcion es capaz de añadirlos
    public function getToken($data){
        $time = time();
        if(is_string($data)){
            $rawdata = json_decode($data, true);
        }
        else{
            $rawdata =json_decode(json_encode($data),true);
        }
        $rawdata[0]['iat']=time();
        $rawdata[0]['exp']=time()+3600;
        $resData = json_encode($rawdata);
        $jwt = JWT::encode($rawdata, self::$key, 'HS256');
        return $jwt;
    }
    //Esta funcion acepta como argumento un token en forma de String y devuelve un JSON con la información contenida en el Token
    public function getInfo($token){  
        try{
            $data = JWT::decode($token, self::$key, array('HS256'));
            return json_encode($data);
        }
        catch(\Firebase\JWT\SignatureInvalidException $e){
            return $e->getMessage();
        }
    }
    //Esta funcion recibe un token en forma de String y devuelve un nuevo Token valido con un nuevo tiempo de expiración
    //Se aconseja su uso exclusivamente cuando el token este vencido y los datos de usuario sean validados correctamente
    public function refreshToken($token){
        list($header, $payload, $signature) = explode(".", $token);
        $data = JWT::urlsafeB64Decode($payload);
        $hash = json_decode($data, true)[0]['Hashcode'];
        $refreshTime = self::checkToken($hash);
        if($refreshTime>0){
            $resultado = $this->getToken($data);
        }
        else{
            $resultado = null;
        }
        return $resultado;
    }
    private function checkToken($hash): int{
        $objmysql = Misc::connection();
        if ($objmysql->connect_errno) {
            return 0;
        }
        $hash = Misc::escape($hash, $objmysql);
        $sql = "Select check_token('$hash');";
        $stmt = $objmysql->query($sql);
        $sqlresult = mysqli_fetch_all($stmt, MYSQLI_NUM);
        $respuesta = $sqlresult[0][0];
        return (int)$respuesta;
    }
}

