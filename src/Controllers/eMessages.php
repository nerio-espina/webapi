<?php
namespace App\Controllers;
use \Firebase\JWT\JWT; 
//Controlador de Autorizacion
class eMessages{
    public static function badRequest($response){
        //La solicitud contiene sintaxis erronea y no deberia repetirse
        $json = array("Error:"=>"400", "Message:"=>"Bad Request");
        return $response->withJson($json, 400);
    }
    public static function unauthorized($response){
        //Acceso no autorizado: la autenticacion aun no se ha llevado a cabo o la aplicacion no se encuentra autorizada
        $json = array("Error:"=>"401", "Message:"=>"Unauthorized");
        return $response->withJson($json, 401);
    }
    public static function paymentRequired($response){
        //Es necesario el pago de esta caracteristica para ser utilizada
        $json = array("Error:"=>"402", "Message:"=>"Payment Required");
        return $response->withJson($json, 402);
    }
    public static function forbidden($response){
        //El cliente no tiene permisos necesarios para acceder a esta funcion
        $json = array("Error:"=>"403", "Message:"=>"Forbidden");
        return $response->withJson($json, 403);
    }
    public static function notFound($response){
        //Recurso no encontrado
        $json = array("Error:"=>"404", "Message:"=>"Not Found");
        return $response->withJson($json, 404);
    }
    public static function notAllowed($response){
        //El metodo (POST, GET, etc) de este request no está soportado por esta ruta
        $json = array("Error:"=>"405", "Message:"=>"Not Allowed");
        return $response->withJson($json, 405);
    }
    public static function notAcceptable($response){
        //El metodo (POST, GET, etc) de este request no está soportado por esta ruta
        $json = array("Error:"=>"406", "Message:"=>"Not Acceptable");
        return $response->withJson($json, 406);
    }
    public static function gone($response){
        //El recurso o servidor no está disponible
        $json = array("Error:"=>"410", "Message:"=>"Gone");
        return $response->withJson($json, 410);
    }
    public static function lengthRequired($response){
        //Solicitud rechazada porque no se incluye Content-Lenght en el Request Header.
        $json = array("Error:"=>"411", "Message:"=>"Length Required");
        return $response->withJson($json, 411);
    }
    public static function preconditionFailed($response){
        //El servidor no es capaz de cumplir con algunas de las condiciones impuestas por el navegador en su peticion
        $json = array("Error:"=>"412", "Message:"=>"Precondition Failed");
        return $response->withJson($json, 412);
    }
    public static function tooLarge($response){
        //La solicitud es demasiado grande
        $json = array("Error:"=>"413", "Message:"=>"Request Too Large");
        return $response->withJson($json, 413);
    }
    public static function unavailableLegal($response){
        //El servidor no es capaz de cumplir con algunas de las condiciones impuestas por el navegador en su peticion
        $json = array("Error:"=>"451", "Message:"=>"Unavailable for legal reasons");
        return $response->withJson($json, 451);
    }
}
