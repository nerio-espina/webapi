<?php
namespace App\Controllers;
use \Firebase\JWT\JWT; 
use \App\Controllers\Miscelaneos as Misc;
use \App\Controllers\eMessages as eMsg;
//Controlador de Punto de Venta
class PuntoVenta{
    protected $container;
    public function __construct($container){
        $this->container = $container;
    }
    /****
     * LINEAS, FAMILIAS, SUBFAMILIAS Y PRODUCTOS
     ****/
    //PRODUCTOS
    public function get_favoritos($request, $response, $args){
        $decoder = new \App\Controllers\AutController($this->container);
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        $appData = json_decode($decoder->getInfo($token),true);
        $almacenID = $appData[0]["AlmacenID"];        
        $objmysql = Misc::connection();
        if ($objmysql->connect_errno) {
            return eMsg::gone($response);
        }
        $limit = Misc::escape($args['limit'], $objmysql);
        $sql = "Call sp_lista_favoritos('$almacenID', '$limit');";
        $stmt = $objmysql->query($sql);
        if (mysqli_num_rows($stmt)==0){
            $json = array("Resultado"=>"No hay resultados");
            $respuesta = json_encode($json);   
        }
        else{
            $sqlresult = mysqli_fetch_all($stmt, MYSQLI_ASSOC);
            $respuesta = json_encode($sqlresult);
        }
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($respuesta);
    }
    //Obtiene las lineas de la empresa
    public function get_lineas($request, $response, $args){
        $decoder = new \App\Controllers\AutController($this->container);
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        $appData = json_decode($decoder->getInfo($token),true);       
        $objmysql = Misc::connection();
        if ($objmysql->connect_errno) {
            return eMsg::gone($response);
        }
        $EmpresaID = $appData[0]["EmpresaID"];
        $sql = "Call sp_get_lineas('$EmpresaID');";
        $stmt = $objmysql->query($sql);
        if (mysqli_num_rows($stmt)==0){
            $json = array("Resultado"=>"No hay resultados");
            $respuesta = json_encode($json);   
        }
        else{
            $sqlresult = mysqli_fetch_all($stmt, MYSQLI_ASSOC);
            $respuesta = json_encode($sqlresult);
        }
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($respuesta);
    }
    //obtiene las Familias correspondientes a la linea especificada en el argumento
    public function get_familia($request, $response, $args){
        $decoder = new \App\Controllers\AutController($this->container);
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        $appData = json_decode($decoder->getInfo($token),true);       
        $objmysql = Misc::connection();
        if ($objmysql->connect_errno) {
            return eMsg::gone($response);
        }
        $EmpresaID = $appData[0]["EmpresaID"];
        $LineaID = Misc::escape($args['linea'], $objmysql);
        $sql = "Call sp_get_familia('$LineaID', '$EmpresaID');";
        $stmt = $objmysql->query($sql);
        if (mysqli_num_rows($stmt)==0){
            $json = array("Resultado"=>"No hay resultados");
            $respuesta = json_encode($json);   
        }
        else{
            $sqlresult = mysqli_fetch_all($stmt, MYSQLI_ASSOC);
            $respuesta = json_encode($sqlresult);
        }
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($respuesta);
    }
    //Obtiene las subfamilias correspondientes a la Familia especificada en el argumento
    public function get_subfamilia($request, $response, $args){
        $decoder = new \App\Controllers\AutController($this->container);
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        $appData = json_decode($decoder->getInfo($token),true);       
        $objmysql = Misc::connection();
        if ($objmysql->connect_errno) {
            return eMsg::gone($response);
        }
        $EmpresaID = $appData[0]["EmpresaID"];
        $FamiliaID = Misc::escape($args['familia'], $objmysql);
        $sql = "Call sp_get_subfamilia('$FamiliaID', '$EmpresaID');";
        $stmt = $objmysql->query($sql);
        if (mysqli_num_rows($stmt)==0){
            $json = array("Resultado"=>"No hay resultados");
            $respuesta = json_encode($json);   
        }
        else{
            $sqlresult = mysqli_fetch_all($stmt, MYSQLI_ASSOC);
            $respuesta = json_encode($sqlresult);
        }
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($respuesta);
    }
    //Aqui va igual a favoritos pero con parametro subfamilia
    public function get_productos_subfamilia($request, $response, $args){
        $decoder = new \App\Controllers\AutController($this->container);
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        $appData = json_decode($decoder->getInfo($token),true);
        $almacenID = $appData[0]["AlmacenID"];        
        $objmysql = Misc::connection();
        if ($objmysql->connect_errno) {
            return eMsg::gone($response);
        }
        $limit = Misc::escape($args['limit'], $objmysql);
        $SubfamiliaID = Misc::escape($args['subfamilia'], $objmysql);
        $sql = "Call sp_productos_subfamilia('$almacenID', '$limit', '$SubfamiliaID');";
        $stmt = $objmysql->query($sql);
        if (mysqli_num_rows($stmt)==0){
            $json = array("Resultado"=>"No hay resultados");
            $respuesta = json_encode($json);   
        }
        else{
            $sqlresult = mysqli_fetch_all($stmt, MYSQLI_ASSOC);
            $respuesta = json_encode($sqlresult);
        }
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($respuesta);
    }
    /****
     * OPERACIONES
     ****/
    //Registro de Preventa y detalles
    function post_preventa($request, $response, $args){
       
        $contenido = $request->getParsedBody();
        $initHeaders;
        $headers;
        foreach($contenido as $clave=>$valor){
            if($clave != "Registros" and $clave != "Param_ID_Preventa"){
                $initHeaders[$clave] = $valor;
            }
        }
        $headers = str_replace("'null'", "NULL", "('" . implode("', '", $initHeaders) . "'");
        
        $initValues = (array)$contenido['Registros'];
        $finalValues;
        foreach($initValues as $clave=>$valor){
            $finalValues[$clave] = str_replace('"null"', 'NULL', '("' . implode('", "', $valor) . '")');
        }
        $values = implode(", ",$finalValues);
        $_idpreventa = str_replace("'null'", "NULL", "'" . $contenido['Param_ID_Preventa'] . "'");
        $consulta = "Call sp_preventa_insert" . $headers . ", '" . $values . "', " .  $_idpreventa . ");";

        return $response->withStatus(200)->withHeader('Content-Type', 'text')->write($consulta);
    }
    

}

