<?php
namespace App\Controllers;
use mysqli;
use \App\Controllers\eMessages as eMsg;
use \App\Controllers\Miscelaneos as Miscelaneos;
//Controlador de Funciones Relacionadas a Clientes y Anexos
class Clientes{
    
    static protected $container;
    public function __construct($container){
        self::$container = $container;
    }
    //Devuelve un Json con la informacion EmpresaID, AlmacenID, EstablecimientoID
    function getEmpresaID($request, $response, $args){
        $app = new \Slim\App;
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        $encoder = new \App\Controllers\AutController($app->getContainer());
        $decodificado = json_decode($encoder->getInfo($token), true)[0];
        $info['EmpresaID'] = $decodificado['EmpresaID'];
        $info['EstablecimientoID'] = $decodificado['EmpresaID'];
        $info['AlmacenID'] = $decodificado['AlmacenID'];
        $respuesta = json_encode($info);
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($respuesta);
    }
    //Devuelve un json con los anexos a partir de un filtro
    function getClientes($request, $response, $args){
        $app = new \Slim\App;
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        $encoder = new \App\Controllers\AutController($app->getContainer());
        $decodificado = json_decode($encoder->getInfo($token), true)[0];
        $EmpresaID = $decodificado['EmpresaID'];
        $objmysql = Miscelaneos::connection();
        if ($objmysql->connect_errno) {
            return eMsg::gone($response);
        }
        $campo = Miscelaneos::escape($args['campo'], $objmysql);
        $filtro = Miscelaneos::escape($args['filtro'], $objmysql);
        $sql = "Call sp_get_clientes('$campo', '$filtro', '$EmpresaID');";
      
        $stmt = $objmysql->query($sql);
        if (mysqli_num_rows($stmt)==0){
            $json = array("Resultado"=>"No hay resultados");
            $respuesta = json_encode($json);   
        }
        else{
            $sqlresult = mysqli_fetch_all($stmt, MYSQLI_ASSOC);
            $respuesta = json_encode($sqlresult);
        }
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($respuesta);
        
    }
    function postCliente($request, $response, $args){
        $contenido = $request->getParsedBody();
        $response->write(json_encode($contenido));
        return $response->withHeader('Content-Type', 'application/json');
    }


}
