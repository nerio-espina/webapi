<?php
namespace App\Controllers;
require_once __DIR__ . '/../../phpqrcode/qrlib.php';
use mysqli;
use \App\Controllers\eMessages as eMsg;
//Controlador de Funciones Miscelaneas
class Miscelaneos{
    
    static protected $container;
    public function __construct($container){
        self::$container = $container;
    }
    public static function connection(){
        $dbhost = "100.10.10.108:3306";
        $dbname = "pruebadb";
        $dbuser = "root"; //"root";
        $dbpass = "Procesos@2017";//"Procesos@2018";
        $dbh = new mysqli($dbhost,$dbuser, $dbpass, $dbname);
        return $dbh;
    }
    public static function escape($string, $objetomysqli){
        return $objetomysqli->real_escape_string($string);
    }
    public static function getAppToken($request, $response, $args){
        $objmysql = Miscelaneos::connection();
        $sql = "Select * from tbl_app;";
        $stmt = $objmysql->query($sql);
        $app = mysqli_fetch_all($stmt, MYSQLI_ASSOC);
        $data = json_encode($app);
        $encoder = new \App\Controllers\AutController(self::$container);
        $token = $encoder->getToken($app);
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($token);
    }
    function qrgenerator($request, $response, $args){
        $valor = (string)$args['value'];
        try{
            $img = \QRCode::png($valor);
            return $img;
        } catch(Exception $e){
            echo '{"error":{"text":'. $e->getMessage() . '}}';
        }
    }
    function qrgeneratorpost($request, $response, $args){
        $json = $request->getParsedBody();
        $valor = $json['contenido'];
        try{
            $img = \QRCode::png($valor);
            $imgn = new \Imagick($img);
            return base64_encode($imgn);
        } catch(Exception $e){
            echo '{"error":{"text":'. $e->getMessage() . '}}';
        }
    }
    function getEntidades($request, $response, $args){
        $objmysql = Miscelaneos::connection();
        if ($objmysql->connect_errno) {
            return eMsg::gone($response);
        }
        $sqlProvincias = "Select id_prov as id, provincia, id_depa From tbl_provincias;";
        $sqlDistritos = "Select id_dist as id, distrito, id_prov, ubigeo From tbl_distritos;";
        $sqlPaises = "Select idpais as id, nompais as pais, codpais_a2, codpais_a3, codnumerico From tbl_paises;";
        $sqlDepartamentos = "Select id_depa as id, departamento from tbl_departamentos;";
        $stmtProvincias = $objmysql->query($sqlProvincias);
        $stmtDistritos = $objmysql->query($sqlDistritos);
        $stmtPaises = $objmysql->query($sqlPaises);
        $stmtDepartamentos = $objmysql->query($sqlDepartamentos);

        if (mysqli_num_rows($stmtProvincias)==0 or mysqli_num_rows($stmtDistritos)==0 or mysqli_num_rows($stmtPaises)==0 or mysqli_num_rows($stmtDepartamentos)==0){
            $json = array("Resultado"=>"No hay resultados");
            $respuesta = json_encode($json);   
        }
        else{
            while ($fila = $stmtPaises->fetch_assoc()) {
                $nombrefila = $fila['pais'];
                $resultPaises[$nombrefila]=$fila;
            }
            while ($fila = $stmtProvincias->fetch_assoc()) {
                $nombrefila = $fila['provincia'];
                $resultProvincias[$nombrefila]=$fila;
            }
            while ($fila = $stmtDistritos->fetch_assoc()) {
                $nombrefila = $fila['distrito'];
                $resultDistritos[$nombrefila]=$fila;
            }
            while ($fila = $stmtDepartamentos->fetch_assoc()) {
                $nombrefila = $fila['departamento'];
                $resultDepartamentos[$nombrefila]=$fila;
            }
            $resultadoGlobal = array("Paises"=>$resultPaises, "Departamentos"=>$resultDepartamentos, "Provincias"=>$resultProvincias, "Distritos"=>$resultDistritos);
            $respuesta = json_encode($resultadoGlobal);

        }
        return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($respuesta);
    }

}
