<?php
//Controlador de Inicio de Sesion y Registro
namespace App\Controllers;
use \Firebase\JWT\JWT;
//Importante: Para realizar consultas debemos importar la siguiente clase: 
use \App\Controllers\Miscelaneos as Misc;
use \App\Controllers\eMessages as eMsg;
class Sign{
    
    protected $container;
    public function __construct($container){
        $this->container = $container;
    }
    public function login($request, $response, $args){
        //Declaro el objeto encriptador-desencriptador de informacion
        $decoder = new \App\Controllers\AutController($this->container);
        //Obtengo el Token de la APP
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        //obtengo el ID de la APP
        $appData = json_decode($decoder->getInfo($token),true);
        $appDataID = $appData[0]["idApp"];
        //Creo un objeto MYSQLI que me permita usar ESCAPE_STRING y la consulta
        $objmysql = Misc::connection();
        if ($objmysql->connect_errno) {
            return eMsg::gone($response);
        }
        //Usar la funcion Escape del objeto anterior con parametros: String y Objeto MYSQLI
        $user = Misc::escape($args['user'], $objmysql);
        $password = Misc::escape($args['password'], $objmysql);
        //Construyo la consulta
        $sql = "Call sp_login('$user', '$password', '$appDataID');";
        //Ejecuto la consulta mediante la funcion QUERY del Objeto MYSQLI con el parametro CONSULTA. El resultado lo asigno a una variable
        $stmt = $objmysql->query($sql);
        //Utilizo el resultado y lo almaceno en un objeto mediante la funcion mysqli_fetch_all con parametros $Resultado y "MYSQLI_ASSOC"
        $sqlresult = mysqli_fetch_all($stmt, MYSQLI_ASSOC);
        if(!$sqlresult || $sqlresult[0]['Resultado'] == "NO AUTORIZADO"){
            $json = array("Resultado"=>"No Autorizado");
            $respuesta = json_encode($json);
        }
        else{
            $user = json_encode($sqlresult);
            $respuesta = $decoder->getToken($user);
        }
        return $response->withStatus(200)->withHeader('Content-Type', 'text')->write($respuesta);
    }
    
}