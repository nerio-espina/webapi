<?php
namespace App\Middleware;
use App\Controllers;
use App\Controllers\eMessages as msg;
use \App\Controllers\Miscelaneos as Misc;
//Middleware encargado de verificar la legitimidad de la Aplicacion
class SecureApp{
    protected $container;
    public function __construct($container){
        $this->container = $container;
    }
    public function __invoke($request, $response, $next){
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];

        if($this->auth($token)){
            return $next($request, $response); 
        }
        else{
            return msg::forbidden($response);
        }
     }
     private function appReview($token){
         $decoder = new \App\Controllers\AutController($this->container);
         $userdata = $decoder->getInfo($token);      
         if($userdata->data == "NerioAPP"){
             return True;
         }
         return False;
     }
     

}