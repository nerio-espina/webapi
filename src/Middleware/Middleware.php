<?php
namespace App\Middleware;
use App\Controllers;
use App\Controllers\eMessages as msg;
use App\Controllers\Miscelaneos as Misc;
//Guardia comprobador del Hash contenido en el Token 
class Middleware{
    //Declaramos el container y luego le asignamos valor en la funcion __construct()
    protected $container;
    public function __construct($container){
        $this->container = $container;
    }
    //La función __invoke contiene los metodos a ejecutar por parte del middleware antes de continuar o abortar la tarea
    public function __invoke($request, $response, $next){
        $token = json_decode(json_encode($request->getHeader('Authorization')), true)[0];
        if($token ==""){
            return msg::forbidden($response);
        }
        if($this->auth($token)){
        
            return $next($request, $response); 
        }
        else{
            return msg::forbidden($response);
        }
     }
     //Funcion auxiliar de tipo Booleano, que luego de realizar comprobaciones determina si el middleware puede continuar o abortar.
     private function auth($token){
        
        $decoder = new \App\Controllers\AutController($this->container);
        $objmysql = Misc::connection();
        if ($objmysql->connect_errno) {
            return false;
        }
        $userdata = json_decode($decoder->getInfo($token),TRUE);
        $idAplicacion = Misc::escape($userdata[0]['AplicacionID'],$objmysql);
        $idUsuario = Misc::escape($userdata[0]['UsuarioID'],$objmysql);
        $hashCode = Misc::escape($userdata[0]['Hashcode'],$objmysql);
        $sql = "Select check_token('$hashCode', '$idUsuario', '$idAplicacion');";
        $stmt = $objmysql->query($sql);
        $result = mysqli_fetch_all($stmt, MYSQLI_NUM);
        if($result[0][0] == "0"){
            return false;
        }else
        {
            return true;
        }
    }


}
