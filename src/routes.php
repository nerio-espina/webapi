<?php
use Slim\Http\Request;
use Slim\Http\Response;
use \Firebase\JWT\JWT; 
use App\Controllers;
use App\Controllers\Miscelaneos;
use App\Middleware\Middleware as Middleware;
//Esta seccion agrupa las diferentes rutas, en cascada, a utilizar por nuestra API. 
$app->group('/api', function() use ($app){
    $app->group('/v1', function() use($app){
        //SIGN
        $app->get('/login/{user:.+}/{password:.+}', 'Sign:login');

        //MISCELANEOS
        $app->get('/qrgen/{value:.+}', 'Miscelaneos:qrgenerator')->add(new Middleware($app->getContainer()));
        $app->post('/qrgen1', 'Miscelaneos:qrgeneratorpost')->add(new Middleware($app->getContainer()));
        $app->get('/app/getToken/{id:.+}', 'Miscelaneos:getAppToken')->add(new Middleware($app->getContainer()));
        $app->get('/entidades', 'Miscelaneos:getEntidades')->add(new Middleware($app->getContainer()));
        //CLIENTES
        $app->get('/getids', 'Clientes:getEmpresaID')->add(new Middleware($app->getContainer()));
        $app->get('/clientes/{campo:.+}/{filtro:.+}', 'Clientes:getClientes')->add(new Middleware($app->getContainer()));
        $app->post('/clientes', 'Clientes:postCliente')->add(new Middleware($app->getContainer()));

        //PUNTO DE VENTA
        $app->get('/favoritos/{limit:.+}', 'PuntoVenta:get_favoritos')->add(new Middleware($app->getContainer()));
        $app->get('/lineas', 'PuntoVenta:get_lineas')->add(new Middleware($app->getContainer()));
        $app->get('/familia/{linea:.+}', 'PuntoVenta:get_familia')->add(new Middleware($app->getContainer()));
        $app->get('/subfamilia/{familia:.+}', 'PuntoVenta:get_subfamilia')->add(new Middleware($app->getContainer()));
        $app->get('/productossubfamilia/{limit:.+}/{subfamilia:.+}', 'PuntoVenta:get_productos_subfamilia')->add(new Middleware($app->getContainer()));
        $app->post('/postpreventa', 'PuntoVenta:post_preventa')->add(new Middleware($app->getContainer()));
        //DEV
        $app->get('/getinfo/{token:.+}', 'getinfo')->add(new Middleware($app->getContainer()));
        $app->get('/refreshtoken/{token:.+}', 'refreshToken');
    });
});

//FUNCIONES DE PRUEBA DE DESARROLLO
function getinfo($request, $response, $args){
    $app = new Slim\App;
    $token = $args['token'];
    $encoder = new \App\Controllers\AutController($app->getContainer());
    $stmt = $encoder->getInfo($token);
    return $stmt;
};
function refreshToken($request, $response, $args){
    $app = new Slim\App;
    $token = $args['token'];
    $encoder = new \App\Controllers\AutController($app->getContainer());
    $stmt = $encoder->refreshToken($token);
    return $stmt;
};
function encodeToken($request, $response, $args){
    $app = new Slim\App;
    $_POST = $request->getParsedBody();
    $encoder = new \App\Controllers\AutController($app->getContainer());
    $stmt = $encoder->getToken($_POST);
    
    return $stmt;
};
