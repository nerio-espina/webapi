<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

//MIDDLEWAREs
$container['Middleware'] = function($container){
    return new \App\Middleware\Middleware($container);
};
$container['SecureApp'] = function($container){
    return new \App\Middleware\SecureApp($container);
};

//CONTROLLERs
//Autorizacion
$container['AutController'] = function($container){
    return new \App\Controllers\AutController($container);
};
//Sign In & Sign Up
$container['Sign'] = function($container){
    return new \App\Controllers\Sign($container);
};
//Miscelaneos
$container['Miscelaneos'] = function($container){
    return new \App\Controllers\Miscelaneos($container);
};
//Mensajes de Errores
$container['eMessages'] = function($container){
    return new \App\Controllers\eMessages($container);
};
//Punto de Venta
$container['PuntoVenta'] = function($container){
    return new \App\Controllers\PuntoVenta($container);
};
//Clientes
$container['Clientes'] = function($container){
    return new \App\Controllers\Clientes($container);
};